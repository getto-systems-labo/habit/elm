module GettoHabit.App.Index.EntryPoint exposing (main)
import GettoHabit.App.Index.Page as Page

import Browser

main = Browser.element
  { init          = Page.init
  , subscriptions = Page.subscriptions
  , update        = Page.update
  , view          = Page.view
  }
